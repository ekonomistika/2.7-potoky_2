import java.util.ArrayList;

class BrokerThread extends Thread {
    Trader name;
    Trader market = new Trader("Market");


    public BrokerThread(Trader name) {
        this.name = name;
        market.setStock(new Stock("AAPL", 100, 141));
        market.setStock(new Stock("COKE", 1000, 387));
        market.setStock(new Stock("IBM", 200, 137));
    }

    @Override
    public void run() {

        ArrayList<Stock> nameStock = name.getStock();
        ArrayList<Stock> marketStock = market.getStock();

        try {
            for (Stock namestock : nameStock) {
                for (Stock marketstock : marketStock) {
                    if (namestock.name == marketstock.name || namestock.price > marketstock.price) {

                            System.out.println("Трейдер " + name.getName() + " покупает акции " + marketstock.name);

                    }else {System.out.println("Трейдер " + name.getName() + " не покупает акции " + marketstock.name);}
                }
                sleep(2000);
            }

        } catch (InterruptedException exception) {
        }
    }
}
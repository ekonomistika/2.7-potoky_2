import java.util.ArrayList;
import java.util.HashMap;

public class Trader {
    public String name;
    public ArrayList<Stock> stock;

    public Trader(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Stock> getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock.add(stock);
    }
}
